import pandas as pd
import requests
import json
import time

GOOGLEMAP_ENDPOINT = 'https://maps.googleapis.com/maps/api/geocode/json'
MAP_QUEST_ENPOINT = 'http://www.mapquestapi.com/geocoding/v1/address'

employee_address = pd.read_csv('./Employee_Addresses.csv')
bust_stops = pd.read_csv('./Potentail_Bust_Stops.csv')

def get_intersection_location(row):
    # Get latitude and longitude from 2 streets intersection
    street1, street2 = row['Street_One'], row['Street_Two']
    params = {
        'address': street1 + "," + street2,
    }
    while True:
        try:
            response= requests.get(GOOGLEMAP_ENDPOINT, params= params)
            response = json.loads(response.text)
            time.sleep(5)
            data = response['results'][0]['geometry']['location']
            return data['lat'], data['lng']
        except Exception:
            pass


def get_location_from_address(row):
    # Get latitude and longitude from an address
    address = row['address']
    params = {
        'key': 'KEY',
        'location': address,
        'maxResults': 1
    }
    while True:
        try:
            response = requests.get(MAP_QUEST_ENPOINT, params=params, )
            response = json.loads(response.text)
            data = response['results'][0]['locations'][0]['latLng']
            time.sleep(1)
            return data['lat'], data['lng']
        except Exception:
            pass

# Apply function to get lat and long and store to csv file
pbs = bust_stops.apply(lambda row: get_intersection_location(row), axis=1)
pbs.to_csv('Bust_Stops_Geometry.csv')
eas = employee_address.apply(lambda row: get_location_from_address(row), axis=1)
eas.to_csv('Employee_Addresses_Geometry.csv')