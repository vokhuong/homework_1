import pandas as pd
import folium

def normalize_lat_long(row):
    # convert lat-long string to float
    record = row.translate(None, '()')
    elems = record.split(',')
    return (float(elems[0]), float(elems[1]))

# Load data from csv file
employee_addresses = pd.read_csv('./Employee_Addresses.csv')
bust_stops = pd.read_csv('./Potentail_Bust_Stops.csv')
employee_address_geometries = pd.read_csv('./Employee_Addresses_Geometry.csv', names=['index', 'lat-long'])
bust_stop_intersection_geometries = pd.read_csv('./Bust_Stops_Geometry.csv', names=['index', 'lat-long'])


employee_addresses['lat-long'] = employee_address_geometries['lat-long'].apply(lambda row: normalize_lat_long(row))
bust_stops['lat-long'] = bust_stop_intersection_geometries['lat-long'].apply(lambda row: normalize_lat_long(row))

SF_COORDINATES = (37.76, -122.45)
map = folium.Map(location=SF_COORDINATES, zoom_start=12)
bust_stops.apply(lambda row: folium.Marker(location=[row['lat-long'][0], row['lat-long'][1]]).add_to(map), axis=1)
employee_addresses.apply(
    lambda row: folium.CircleMarker(location=[row['lat-long'][0], row['lat-long'][1]], radius=6, fill_color='red',
                                    color='red', fill_opacity=1).add_to(map), axis=1)

map.save('visualization_map.html')
